package org.bitbucket.uellee.grumpyapp.presentation.listscreen

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_story.*
import org.bitbucket.uellee.grumpyapp.R
import org.bitbucket.uellee.grumpyapp.domain.entity.Feed
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class StoryFragment : Fragment(), StoryPresenter.StoryView {

    private val presenter: StoryPresenter by inject { parametersOf(this) }

    private val adapter = StoryAdapter(presenter::onBindPaginationLoading, presenter::onFeedClick)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_story, container, false)

    override fun addItems(itemsList: List<Feed>) {
        adapter.addItems(itemsList)
    }

    override fun hideStoryLoading() {
        c_loading_story.isVisible = false
    }

    override fun hidePaginationLoading() {
        adapter.hidePaginationLoading()
    }

    override fun hideStoryList() {
        rv_story.isVisible = false
    }

    override fun showStoryList() {
        rv_story.isVisible = true
    }

    override fun showEmptyState() {
        c_empty_state.isVisible = true
    }

    override fun clearItemsList() {
        adapter.clearItemsList()
    }

    override fun showLoadingErrorDialog() {
        Toast.makeText(context, "Error occured", Toast.LENGTH_LONG).show()
    }

    override fun onStart() {
        super.onStart()
        rv_story.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rv_story.adapter = adapter
        presenter.onViewStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}

var View.isVisible: Boolean
    get() = this.visibility == View.VISIBLE
    set(value) {
        this.visibility =
                if (value) View.VISIBLE
                else View.GONE
    }
