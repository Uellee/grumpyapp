package org.bitbucket.uellee.grumpyapp.domain.entity

data class Feed(
    val title: String,
    val publicationDate: String,
    val link: String,
    val imageLink: String,
    val description: String
)