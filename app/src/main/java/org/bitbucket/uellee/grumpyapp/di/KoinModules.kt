package org.bitbucket.uellee.grumpyapp.di

import okhttp3.OkHttpClient
import org.bitbucket.uellee.grumpyapp.data.RssDataSource
import org.bitbucket.uellee.grumpyapp.data.RssDataSourceImpl
import org.bitbucket.uellee.grumpyapp.data.RssRepositoryImpl
import org.bitbucket.uellee.grumpyapp.domain.RssRepository
import org.bitbucket.uellee.grumpyapp.domain.StoryInteractor
import org.bitbucket.uellee.grumpyapp.domain.StoryUseCase
import org.bitbucket.uellee.grumpyapp.presentation.Router
import org.bitbucket.uellee.grumpyapp.presentation.RouterImpl
import org.bitbucket.uellee.grumpyapp.presentation.details.DetailsPresenter
import org.bitbucket.uellee.grumpyapp.presentation.launch.LaunchPresenter
import org.bitbucket.uellee.grumpyapp.presentation.listscreen.StoryPresenter
import org.koin.dsl.module.module

const val CONTAINER_ID = "CONTAINER_ID"
const val FRAGMENT_MANAGER = "FRAGMENT_MANAGER"

val appModule = module {

    single { OkHttpClient() }

    //app
    single { RouterImpl(getProperty(FRAGMENT_MANAGER), getProperty(CONTAINER_ID)) as Router }

    //presentation
    factory { (view: LaunchPresenter.LaunchView) -> LaunchPresenter(view, get()) }
    factory { (view: StoryPresenter.StoryView) -> StoryPresenter(view, get(), get()) }
    factory { (view: DetailsPresenter.DetailsView) -> DetailsPresenter(view) }

    //domain
    factory { StoryInteractor(get()) as StoryUseCase }

    //data
    factory { RssDataSourceImpl(get()) as RssDataSource }
    factory { RssRepositoryImpl(get()) as RssRepository }
}