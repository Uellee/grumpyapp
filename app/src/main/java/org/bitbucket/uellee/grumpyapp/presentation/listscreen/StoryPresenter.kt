package org.bitbucket.uellee.grumpyapp.presentation.listscreen

import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import org.bitbucket.uellee.grumpyapp.domain.StoryUseCase
import org.bitbucket.uellee.grumpyapp.domain.entity.Feed
import org.bitbucket.uellee.grumpyapp.domain.entity.Filter
import org.bitbucket.uellee.grumpyapp.presentation.FeedDetailsScreen
import org.bitbucket.uellee.grumpyapp.presentation.Router

class StoryPresenter(
    private val view: StoryView,
    private val storyUseCase: StoryUseCase,
    private val router: Router
) {

    private companion object {
        const val NO_OFFSET = 0
    }

    private val disposable = CompositeDisposable()

    private var filter = Filter.DEFAULT

    private val isFirstChunk: Boolean
        get() = filter.offset == NO_OFFSET

    fun onBindPaginationLoading() {
        loadChunk()
    }

    fun onViewStart() {
        filter = Filter.DEFAULT
        loadChunk()
    }

    private fun loadChunk() {
        getData().subscribeBy(
            onSuccess = ::onLoadSucceeded,
            onError = ::onLoadFailed
        ).addTo(disposable)
    }

    private fun getData() =
        storyUseCase.getStoriesListSingle(filter)
            .observeOn(AndroidSchedulers.mainThread())

    private fun onLoadSucceeded(itemsList: List<Feed>) {
        val isLastChunk = itemsList.size < filter.amount

        if (isFirstChunk) view.clearItemsList()

        if (isFirstChunk && itemsList.isEmpty()) onEmptyState()

        if (itemsList.isNotEmpty()) onDataReceived(itemsList)

        if (isLastChunk) onLastChunk()
    }

    private fun onDataReceived(itemsList: List<Feed>) {
        view.addItems(itemsList)

        if (isFirstChunk) {
            view.hideStoryLoading()
            view.showStoryList()
        }

        val newOffset = filter.offset + itemsList.size
        filter = filter.copy(offset = newOffset)
    }

    private fun onLastChunk() {
        view.hidePaginationLoading()
    }

    private fun onEmptyState() {
        view.showEmptyState()
        view.hideStoryLoading()
        view.hideStoryList()
    }

    private fun onLoadFailed(t: Throwable) {

        Log.d("%%%", "${t.message}\n${t.stackTrace.forEach { println(it.toString()) }}")

        view.showLoadingErrorDialog()
    }

    fun onFeedClick(feed: Feed) {
        router.navigateTo(FeedDetailsScreen(feed))
    }

    fun onDestroy() {
        disposable.dispose()
    }

    interface StoryView {
        fun hideStoryList()
        fun showStoryList()

        fun addItems(itemsList: List<Feed>)
        fun hideStoryLoading()
        fun hidePaginationLoading()
        fun showEmptyState()
        fun showLoadingErrorDialog()
        fun clearItemsList()
    }
}