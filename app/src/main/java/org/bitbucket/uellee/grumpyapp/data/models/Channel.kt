package org.bitbucket.uellee.grumpyapp.data.models

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false)
class Channel {
    @field:ElementList(inline = true, type = FeedItem::class)
    @JvmField
    var feedItemList: MutableList<FeedItem> = mutableListOf()
}