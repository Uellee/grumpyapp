package org.bitbucket.uellee.grumpyapp.data

import io.reactivex.Single
import org.bitbucket.uellee.grumpyapp.data.models.FeedItem
import org.bitbucket.uellee.grumpyapp.domain.RssRepository
import org.bitbucket.uellee.grumpyapp.domain.entity.Feed
import org.bitbucket.uellee.grumpyapp.domain.entity.Filter

class RssRepositoryImpl(private val rssDataSource: RssDataSource) : RssRepository {
    override fun getFeedListSingle(filter: Filter): Single<List<Feed>> =
        rssDataSource.getItems(filter.offset, filter.amount)
            .map(::convert)

    private fun convert(list: List<FeedItem>): List<Feed> =
        list.map(::convert)

    private fun convert(feedItem: FeedItem): Feed =
        feedItem.run {
            Feed(
                title,
                pubDate,
                link,
                enclosure.url,
                description
            )
        }

}