package org.bitbucket.uellee.grumpyapp.data.models

import org.bitbucket.uellee.grumpyapp.domain.EMPTY_STRING
import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

@Root(strict = false)
class Enclosure {
    @field:Attribute
    @JvmField
    var url: String = EMPTY_STRING
}