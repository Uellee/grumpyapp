package org.bitbucket.uellee.grumpyapp.domain.entity

data class Filter(val offset: Int, val amount: Int) {
    companion object {
        val DEFAULT = Filter(offset = 0, amount = 20)
    }
}