package org.bitbucket.uellee.grumpyapp.data

import android.util.Log
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import org.bitbucket.uellee.grumpyapp.data.models.FeedItem
import org.bitbucket.uellee.grumpyapp.data.models.RssContainer
import org.simpleframework.xml.Serializer
import org.simpleframework.xml.core.Persister
import java.io.IOException

interface RssDataSource {
    fun getItems(offset: Int, amount: Int): Single<List<FeedItem>>
}

class RssDataSourceImpl(private val client: OkHttpClient) : RssDataSource {

    private companion object {
        const val URL = "https://www.liga.net/tech/technology/rss.xml"
    }

    private val serializer: Serializer = Persister()


    override fun getItems(offset: Int, amount: Int): Single<List<FeedItem>> = loadList()

    private fun loadList(): Single<List<FeedItem>> =
        Single.just(URL)
            .map(::makeRequest)
            .map(::convertXml)
            .subscribeOn(Schedulers.io())

    private fun convertXml(xml: String): List<FeedItem> {
        val rss = serializer.read(RssContainer::class.java, xml)

        Log.d("%%%", "$rss")

        return rss.channel.feedItemList
    }

    private fun makeRequest(url: String): String {
        val request = Request.Builder()
            .url(url)
            .build()

        val response = client.newCall(request).execute()

        if (!response.isSuccessful) throw IOException("Request failed: ${response.code()}")
        return response.body()?.string() ?: throw NullPointerException("Response body is null")
    }
}