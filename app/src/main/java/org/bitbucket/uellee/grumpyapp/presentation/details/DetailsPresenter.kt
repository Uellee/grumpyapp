package org.bitbucket.uellee.grumpyapp.presentation.details

class DetailsPresenter(private val view: DetailsView) {

    var link: String = ""
        set(value) {
            view.navigateTo(value)
            field = value
        }

    fun onPageFinished() {
        view.showWebPage()
        view.hideProgress()
    }

    interface DetailsView {
        fun navigateTo(link: String)
        fun showWebPage()
        fun hideProgress()
    }
}