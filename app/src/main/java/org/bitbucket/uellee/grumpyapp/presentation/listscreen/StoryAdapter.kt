package org.bitbucket.uellee.grumpyapp.presentation.listscreen

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.vh_item.view.*
import org.bitbucket.uellee.grumpyapp.R
import org.bitbucket.uellee.grumpyapp.domain.entity.Feed

class StoryAdapter(
    private val onBindLoadingItem: () -> Unit,
    private val onFeedClick: (Feed) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private companion object {
        const val LOADING_VIEW_TYPE = 1
        const val ITEM_VIEW_TYPE = 2
    }

    private val loading = Loading()
    private val itemsList: MutableList<Any> = mutableListOf(loading)

    private val positionBeforeLoading
        get() = itemsList.indexOf(loading)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            LOADING_VIEW_TYPE -> LoadingVH.create(parent)
            ITEM_VIEW_TYPE -> ItemVH.create(parent)
            else -> throw IllegalArgumentException("Unhandled vh type: $viewType")
        }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
//надо ли действительно устанавивать onClick здесь, если вместе с фрагментов потрутся презентер, интерактор и пр.
        if (holder is ItemVH) {
            holder.itemView
                .setOnClickListener {
                    onFeedClick(holder.feed!!)
                }
        }
    }

    override fun getItemCount(): Int = itemsList.size

    override fun onBindViewHolder(vh: RecyclerView.ViewHolder, position: Int) {
        if (vh is ItemVH) vh.feed = itemsList[position] as Feed
        else onBindLoadingItem()
    }

    fun addItems(newItemsList: List<Feed>) {
        val insertPosition = positionBeforeLoading
        itemsList.addAll(insertPosition, newItemsList)

        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int =
        if (itemsList[position] is Loading) LOADING_VIEW_TYPE
        else ITEM_VIEW_TYPE

    fun hidePaginationLoading() {
        itemsList.remove(loading)
        notifyItemRemoved(itemsList.size)
    }

    fun clearItemsList() {
        itemsList.clear()
        itemsList.add(loading)

        notifyDataSetChanged()
    }

    class ItemVH(view: View) : RecyclerView.ViewHolder(view) {

        companion object {
            fun create(parent: ViewGroup): ItemVH = ItemVH(parent.inflate(R.layout.vh_item))
        }

        private val glide = Glide.with(itemView)

        private val requestOptions = RequestOptions().apply {
            error(R.drawable.ic_broken_image)
            fitCenter()
        }

        var feed: Feed? = null
            set(value) {
                if (value == null) return

                glide.load(value.imageLink)
                    .apply(requestOptions)
                    .into(itemView.i_preview)

                itemView.tv_title.text = value.title
                itemView.tv_body.text = value.description

                field = value
            }
    }

    private class Loading

    class LoadingVH(view: View) : RecyclerView.ViewHolder(view) {
        companion object {
            fun create(parent: ViewGroup): LoadingVH = LoadingVH(parent.inflate(R.layout.vh_loading))
        }
    }
}

fun ViewGroup.inflate(res: Int): View = LayoutInflater.from(context).inflate(res, this, false)