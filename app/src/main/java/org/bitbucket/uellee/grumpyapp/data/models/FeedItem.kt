package org.bitbucket.uellee.grumpyapp.data.models

import org.bitbucket.uellee.grumpyapp.domain.EMPTY_STRING
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "item")
class FeedItem {
    @field:Element
    @JvmField
    var title: String = EMPTY_STRING
    @field:Element
    @JvmField
    var link: String = EMPTY_STRING
    @field:Element
    @JvmField
    var description: String = EMPTY_STRING
    @field:Element
    @JvmField
    var pubDate: String = EMPTY_STRING
    @field:Element
    @JvmField
    var enclosure: Enclosure =
        Enclosure()
}