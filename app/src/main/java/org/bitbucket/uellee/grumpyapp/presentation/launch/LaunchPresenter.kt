package org.bitbucket.uellee.grumpyapp.presentation.launch

import org.bitbucket.uellee.grumpyapp.presentation.Router
import org.bitbucket.uellee.grumpyapp.presentation.StoryScreen

class LaunchPresenter(private val view: LaunchView, private val router: Router) {

    fun onBackPressed() {
        if (router.onBackPressed()) view.throwBackPressed()
    }

    fun onViewIsShown() {
        router.navigateTo(StoryScreen())
    }

    interface LaunchView {
        fun throwBackPressed()
    }
}