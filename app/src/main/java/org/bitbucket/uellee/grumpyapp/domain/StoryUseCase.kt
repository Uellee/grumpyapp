package org.bitbucket.uellee.grumpyapp.domain

import io.reactivex.Single
import org.bitbucket.uellee.grumpyapp.domain.entity.Feed
import org.bitbucket.uellee.grumpyapp.domain.entity.Filter

interface StoryUseCase {
    fun getStoriesListSingle(filter: Filter): Single<List<Feed>>
}

class StoryInteractor(private val rssRepository: RssRepository) : StoryUseCase {
    override fun getStoriesListSingle(filter: Filter): Single<List<Feed>> =
        rssRepository.getFeedListSingle(filter)
}