package org.bitbucket.uellee.grumpyapp.presentation

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import org.bitbucket.uellee.grumpyapp.domain.entity.Feed
import org.bitbucket.uellee.grumpyapp.presentation.details.DetailsFragment
import org.bitbucket.uellee.grumpyapp.presentation.listscreen.StoryFragment

interface Router {
    fun navigateTo(screen: Screen)
    fun onBackPressed(): Boolean
}

sealed class Screen

class FeedDetailsScreen(val feed: Feed) : Screen()
class StoryScreen : Screen()

class RouterImpl(private val fragmentManager: FragmentManager, private val container: Int) : Router {
    override fun navigateTo(screen: Screen) {
        fragmentManager.beginTransaction()
            .replace(container, createFragment(screen))
            .commit()
    }

    private fun createFragment(screen: Screen): Fragment =
        when (screen) {
            is FeedDetailsScreen -> DetailsFragment.create(screen.feed)
            is StoryScreen -> StoryFragment()
        }

    override fun onBackPressed(): Boolean {
        return when (fragmentManager.findFragmentById(container)) {
            is DetailsFragment -> {
                navigateTo(StoryScreen())
                false
            }

            else -> true
        }
    }
}