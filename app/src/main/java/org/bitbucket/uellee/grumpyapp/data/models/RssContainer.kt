package org.bitbucket.uellee.grumpyapp.data.models

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "rss")
class RssContainer {
    @field:Element
    @JvmField
    var channel: Channel =
        Channel()
}