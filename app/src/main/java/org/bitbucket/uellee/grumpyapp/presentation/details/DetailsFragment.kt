package org.bitbucket.uellee.grumpyapp.presentation.details


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.fragment_webview.*

import org.bitbucket.uellee.grumpyapp.R
import org.bitbucket.uellee.grumpyapp.domain.entity.Feed
import org.bitbucket.uellee.grumpyapp.presentation.FragmentArgumentDelegate
import org.bitbucket.uellee.grumpyapp.presentation.listscreen.isVisible
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class DetailsFragment : Fragment(), DetailsPresenter.DetailsView {

    companion object {
        fun create(feed: Feed): DetailsFragment =
            DetailsFragment().apply {
                link = feed.link
            }
    }

    private var link: String by FragmentArgumentDelegate()

    private val presenter: DetailsPresenter by inject { parametersOf(this) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_webview, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.link = link
    }

    override fun navigateTo(link: String) {
        wv_details.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                presenter.onPageFinished()
            }
        }
        wv_details.loadUrl(link)
    }

    override fun showWebPage() {
        wv_details.isVisible = true
    }

    override fun hideProgress() {
        pb_url_loading.isVisible = false
    }
}