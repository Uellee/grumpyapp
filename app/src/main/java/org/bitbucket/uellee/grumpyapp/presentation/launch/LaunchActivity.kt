package org.bitbucket.uellee.grumpyapp.presentation.launch

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import org.bitbucket.uellee.grumpyapp.R
import org.bitbucket.uellee.grumpyapp.di.CONTAINER_ID
import org.bitbucket.uellee.grumpyapp.di.FRAGMENT_MANAGER
import org.bitbucket.uellee.grumpyapp.di.appModule
import org.bitbucket.uellee.grumpyapp.presentation.Router
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.setProperty
import org.koin.android.ext.android.startKoin
import org.koin.core.parameter.parametersOf
import org.koin.standalone.StandAloneContext

class LaunchActivity : AppCompatActivity(), LaunchPresenter.LaunchView {

    private val presenter: LaunchPresenter by inject { parametersOf(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        StandAloneContext.stopKoin()
        startKoin(applicationContext, listOf(appModule))

        setProperty(CONTAINER_ID, R.id.container_main)
        setProperty(FRAGMENT_MANAGER, supportFragmentManager)


        presenter.onViewIsShown()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    override fun throwBackPressed() {
        super.onBackPressed()
    }
}
